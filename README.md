# Low Memory Monitor

The Low Memory Monitor is an early boot daemon that will monitor memory
pressure information coming from the kernel, and, first, send a signal
to user-space applications when memory is running low, and then activate
the kernel's OOM killer when memory is running really low.

## Background and other uses

- [Tracking pressure-stall information (LWN)](https://lwn.net/Articles/759781/) ([patch](https://lwn.net/Articles/783520/))
- [Upstream documentation](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/tree/Documentation/accounting/psi.rst)
- [system-wide psi monitor and OOM helper](https://github.com/endlessm/eos-boot-helper/tree/master/psi-monitor)
- [Android's lowmemorykiller daemon](https://source.android.com/devices/tech/perf/lmkd) ([source](https://android.googlesource.com/platform/system/core/+/master/lmkd))
- [WebKit's memory pressure monitor](https://github.com/WebKit/webkit/blob/master/Source/WebKit/UIProcess/linux/MemoryPressureMonitor.cpp)

## Dependencies

Requires Linux 5.2 or newer and GLib.
