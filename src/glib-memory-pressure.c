/*
 * Copyright (c) 2019 Bastien Nocera <hadess@hadess.net>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3 as published by
 * the Free Software Foundation.
 *
 */

#include <glib-memory-pressure.h>
#include <glib-unix.h>
#include <errno.h>

/* Constants */
#define MEMORY_PRESSURE_PATH "/proc/pressure/memory"

typedef struct {
	int fd;
	GMPMonitorSourceFunc func;
	gpointer user_data;
} GMPMonitorData;

static void
gmp_monitor_data_free (GMPMonitorData *data)
{
	g_return_if_fail (data != NULL);
	close (data->fd);
	g_free (data);
}

static gboolean
gmp_monitor_fd_event (int          fd,
                      GIOCondition condition,
                      gpointer     user_data)
{
	GMPMonitorData *data = user_data;

	if (condition & G_IO_ERR)
		return FALSE;
	return (* data->func) (data->user_data);
}

guint
gmp_monitor_add (const char            *trigger,
		 GMPMonitorSourceFunc   func,
		 gpointer               user_data,
		 GError               **error)
{
	int fd;
	int ret;
	GMPMonitorData *data;

	g_return_val_if_fail (func != NULL, 0);

	fd = open(MEMORY_PRESSURE_PATH, O_RDWR | O_NONBLOCK);
	if (fd < 0) {
		g_set_error (error,
			     G_UNIX_ERROR, 0,
			     "Could not open %s: %s",
			     MEMORY_PRESSURE_PATH,
			     g_strerror (errno));
		return 0;
	}

	if (write (fd, trigger, strlen(trigger) + 1) < 0) {
		g_set_error (error,
			     G_UNIX_ERROR, 0,
			     "Could not write trigger to %s: %s",
			     MEMORY_PRESSURE_PATH,
			     g_strerror (errno));
		close (fd);
		return 0;
	}

	data = g_new0 (GMPMonitorData, 1);
	data->fd = fd;
	data->func = func;
	data->user_data = user_data;

	ret = g_unix_fd_add_full (G_PRIORITY_DEFAULT,
				  fd,
				  G_IO_PRI | G_IO_ERR,
				  gmp_monitor_fd_event,
				  data,
				  (GDestroyNotify) gmp_monitor_data_free);

	return ret;
}
