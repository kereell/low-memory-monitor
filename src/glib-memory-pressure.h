/*
 * Copyright (c) 2019 Bastien Nocera <hadess@hadess.net>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3 as published by
 * the Free Software Foundation.
 *
 */

#include <glib.h>

typedef gboolean (*GMPMonitorSourceFunc) (gpointer user_data);

guint gmp_monitor_add (const char            *trigger,
		       GMPMonitorSourceFunc   func,
		       gpointer               user_data,
		       GError               **error);
