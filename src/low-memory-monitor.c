/*
 * Copyright (C) 2018 Endless Mobile, Inc.
 * Copyright (c) 2019 Bastien Nocera <hadess@hadess.net>
 *
 * This program is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 3 as published by
 * the Free Software Foundation.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <err.h>
#include <unistd.h>
#include <string.h>

#include "glib-memory-pressure.h"
#include "low-memory-monitor-resources.h"

/* Daemon parameters */
#define POLL_INTERVAL       5
#define RECOVERY_INTERVAL  15
#define OOM_MEM_THRESHOLD  10
#define LOW_MEM_THRESHOLD  20

#define SYSRQ_FILE          "/proc/sys/kernel/sysrq"
#define SYSRQ_TRIGGER_FILE  "/proc/sysrq-trigger"
#define SYSRQ_MASK          0x40
#define BUFSIZE             256

#define LOW_MEMORY_MONITOR_DBUS_NAME          "org.freedesktop.LowMemoryMonitor"
#define LOW_MEMORY_MONITOR_DBUS_PATH          "/org/freedesktop/LowMemoryMonitor"
#define LOW_MEMORY_MONITOR_IFACE_NAME         LOW_MEMORY_MONITOR_DBUS_NAME

typedef struct {
    GMainLoop *loop;
    gint64 last_trigger;
    GDBusNodeInfo *introspection_data;
    GDBusConnection *connection;
    guint name_id;
    int ret;
} LowMemoryMonitorData;

static ssize_t fstr(const char *path, char *rbuf, const char *wbuf) {
    int fd;
    ssize_t n;

    /* one and only one operation per call */
    if ((!rbuf && !wbuf) || (rbuf && wbuf))
        return 0;

    fd = open(path, rbuf ? O_RDONLY : O_WRONLY);
    if (fd < 0)
        err(1, "%s", path);

    if (rbuf)
        n = read(fd, rbuf, BUFSIZE);
    else
        n = write(fd, wbuf, strlen(wbuf));
    if (n < 0)
        err(1, "%s", path);
    close(fd);

    if (rbuf)
        rbuf[n-1] = '\0';

    return n;
}

static void sysrq_trigger_oom() {
    printf("Above threshold limit, killing task and pausing for recovery\n");
    fstr(SYSRQ_TRIGGER_FILE, NULL, "f");
}

static void sysrq_enable_oom() {
    int sysrq;
    char buf[BUFSIZE];

    fstr(SYSRQ_FILE, buf, NULL);
    sysrq = atoi(buf);
    sysrq |= SYSRQ_MASK;
    snprintf(buf, BUFSIZE, "%d", sysrq);
    fstr(SYSRQ_FILE, NULL, buf);
}

static gboolean
oom_cb (gpointer user_data)
{
    LowMemoryMonitorData *data = user_data;
    gint64 current_time;

    current_time = g_get_monotonic_time ();
    if (data->last_trigger == 0 ||
	(current_time - data->last_trigger) > (RECOVERY_INTERVAL * G_USEC_PER_SEC)) {
        sysrq_trigger_oom();
        data->last_trigger = current_time;
    }
    return G_SOURCE_CONTINUE;
}

static gboolean
low_memory_cb (gpointer user_data)
{
    LowMemoryMonitorData *data = user_data;

    g_dbus_connection_emit_signal (data->connection,
				   NULL,
				   LOW_MEMORY_MONITOR_DBUS_PATH,
				   LOW_MEMORY_MONITOR_IFACE_NAME,
				   "LowMemoryWarning",
				   NULL, NULL);

    return G_SOURCE_CONTINUE;
}

static void
name_lost_handler (GDBusConnection *connection,
                   const gchar     *name,
                   gpointer         user_data)
{
    g_debug ("low-memory-monitor is already running, or it cannot own its D-Bus name. Verify installation.");
    exit (0);
}

static const GDBusInterfaceVTable interface_vtable =
{
    NULL,
    NULL,
    NULL
};

static void
bus_acquired_handler (GDBusConnection *connection,
                      const gchar     *name,
                      gpointer         user_data)
{
    LowMemoryMonitorData *data = user_data;

    g_dbus_connection_register_object (connection,
				       LOW_MEMORY_MONITOR_DBUS_PATH,
				       data->introspection_data->interfaces[0],
				       &interface_vtable,
				       data,
				       NULL,
				       NULL);

    data->connection = g_object_ref (connection);
}

static void
name_acquired_handler (GDBusConnection *connection,
		       const gchar     *name,
		       gpointer         user_data)
{
    LowMemoryMonitorData *data = user_data;
    g_autofree char *trigger_oom = NULL;
    g_autofree char *trigger_low_memory = NULL;
    GError *error = NULL;

    trigger_oom = g_strdup_printf ("%s %d %d",
				   "full",
				   POLL_INTERVAL * 1000 * 1000 / 100 * OOM_MEM_THRESHOLD,
				   POLL_INTERVAL * 1000 * 1000);
    if (!gmp_monitor_add (trigger_oom,
			  oom_cb,
			  data,
			  &error)) {
	g_warning ("Failed to add OOM memory pressure monitor: %s", error->message);
	g_error_free (error);
	goto bail;
    }

    trigger_low_memory = g_strdup_printf ("%s %d %d",
					  "full",
					  POLL_INTERVAL * 1000 * 1000 / 100 * LOW_MEM_THRESHOLD,
					  POLL_INTERVAL * 1000 * 1000);
    if (!gmp_monitor_add (trigger_oom,
			  low_memory_cb,
			  NULL,
			  &error)) {
	g_warning ("Failed to add low memory pressure monitor: %s", error->message);
	g_error_free (error);
	goto bail;
    }

    return;

bail:
    data->ret = 0;
    g_main_loop_quit (data->loop);
}

static void
setup_dbus (LowMemoryMonitorData *data)
{
    GBytes *bytes;

    bytes = g_resources_lookup_data ("/org/freedesktop/LowMemoryMonitor/org.freedesktop.LowMemoryMonitor.xml",
				     G_RESOURCE_LOOKUP_FLAGS_NONE,
				     NULL);
    data->introspection_data = g_dbus_node_info_new_for_xml (g_bytes_get_data (bytes, NULL), NULL);
    g_bytes_unref (bytes);
    g_assert (data->introspection_data != NULL);

    data->name_id = g_bus_own_name (G_BUS_TYPE_SYSTEM,
				    LOW_MEMORY_MONITOR_DBUS_NAME,
				    G_BUS_NAME_OWNER_FLAGS_NONE,
				    bus_acquired_handler,
				    name_acquired_handler,
				    name_lost_handler,
				    data,
				    NULL);
}

static void
free_monitor_data (LowMemoryMonitorData *data)
{
    if (data == NULL)
	return;

    if (data->name_id != 0) {
	g_bus_unown_name (data->name_id);
	data->name_id = 0;
    }

    g_clear_pointer (&data->introspection_data, g_dbus_node_info_unref);
    g_clear_object (&data->connection);
    g_clear_pointer (&data->loop, g_main_loop_unref);
    g_free (data);
}

int
main (int argc, char **argv)
{
    LowMemoryMonitorData *data;
    int ret;

    sysrq_enable_oom();

    data = g_new0 (LowMemoryMonitorData, 1);
    setup_dbus (data);

    data->loop = g_main_loop_new (NULL, TRUE);
    g_main_loop_run (data->loop);

    ret = data->ret;
    free_monitor_data (data);

    return ret;
}

/*
 * vim: sw=4 ts=8 cindent noai bs=2
 */
